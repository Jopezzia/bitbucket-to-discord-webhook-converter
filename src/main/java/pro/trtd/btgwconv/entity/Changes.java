package pro.trtd.btgwconv.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Changes {
    private Links links;
    private List<Commit> commits;
    @JsonProperty("new")
    private CommitNew commitNew;

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public List<Commit> getCommits() {
        return commits;
    }

    public void setCommits(List<Commit> commits) {
        this.commits = commits;
    }

    public CommitNew getCommitNew() {
        return commitNew;
    }

    public void setCommitNew(CommitNew commitNew) {
        this.commitNew = commitNew;
    }
}
