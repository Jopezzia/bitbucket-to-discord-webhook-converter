package pro.trtd.btgwconv.entity;

import java.util.List;

public class Push {
    private List<Changes> changes;

    public List<Changes> getChanges() {
        return changes;
    }

    public void setChanges(List<Changes> changes) {
        this.changes = changes;
    }
}
