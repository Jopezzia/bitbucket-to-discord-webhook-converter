package pro.trtd.btgwconv.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Repository {
    @JsonProperty("full_name")
    private String fullName;
    private Links links;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
