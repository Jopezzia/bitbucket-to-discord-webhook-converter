package pro.trtd.btgwconv.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import pro.trtd.btgwconv.dto.BitbucketMessageDTO;
import pro.trtd.btgwconv.dto.DiscordMessageDTO;
import pro.trtd.btgwconv.entity.Commit;
import sun.net.www.http.HttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiscordWorker {
    public static void sendPushMessage(BitbucketMessageDTO bitbucketMessage) {
        new Thread(() -> {
            List<Commit> commits = bitbucketMessage.getPush().getChanges().get(0).getCommits().subList(0, bitbucketMessage.getPush().getChanges().get(0).getCommits().size());
            Collections.reverse(commits);
            DiscordMessageDTO discordMessageDTO = new DiscordMessageDTO(bitbucketMessage);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("https://canary.discordapp.com/api/webhooks/503710535473430529/sM61fi3afybJKZ1esYq1R_wWrhofFL7Ia_JKc3qqsq7lPxp-j8eVGsEg2PuADw5DsMeu");
            List<NameValuePair> params = new ArrayList<>(1);
            params.add(new BasicNameValuePair("payload_json", discordMessageDTO.toJson()));
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}