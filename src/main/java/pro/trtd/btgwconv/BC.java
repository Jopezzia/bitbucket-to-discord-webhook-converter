package pro.trtd.btgwconv;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.trtd.btgwconv.dto.BitbucketMessageDTO;
import pro.trtd.btgwconv.util.DiscordWorker;

@RestController
@RequestMapping("/")
public class BC {

    @PostMapping
    public String test(@RequestBody BitbucketMessageDTO bitbucketMessage){
        DiscordWorker.sendPushMessage(bitbucketMessage);
        return "OK";
    }

}
