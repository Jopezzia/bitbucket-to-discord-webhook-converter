package pro.trtd.btgwconv.dto;

import pro.trtd.btgwconv.entity.Push;
import pro.trtd.btgwconv.entity.Repository;
import pro.trtd.btgwconv.entity.User;

public class BitbucketMessageDTO {
    private Push push;
    private Repository repository;
    private User actor;

    public Push getPush() {
        return push;
    }

    public void setPush(Push push) {
        this.push = push;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public User getActor() {
        return actor;
    }

    public void setActor(User actor) {
        this.actor = actor;
    }
}
