package pro.trtd.btgwconv.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pro.trtd.btgwconv.entity.Commit;
import pro.trtd.btgwconv.entity.Embed;

import java.util.ArrayList;
import java.util.List;

public class DiscordMessageDTO {
    private String content;
    private List<Embed> embeds;

    public DiscordMessageDTO(BitbucketMessageDTO bitbucketMessage){
        this.content = "[" + bitbucketMessage.getActor().getDisplayName() + "](" + bitbucketMessage.getActor().getLinks().getHtml().getHref() + ")"
                + " pushed to " + bitbucketMessage.getPush().getChanges().get(0).getCommitNew().getType()
                + " [" + bitbucketMessage.getPush().getChanges().get(0).getCommitNew().getName() + "]"
                + "(" + bitbucketMessage.getPush().getChanges().get(0).getCommitNew().getLinks().getHtml().getHref() + ")"
                + " of [" + bitbucketMessage.getRepository().getFullName() + "]"
                + "(" + bitbucketMessage.getRepository().getLinks().getHtml().getHref() + ")"
                + " ([Compare changes](" + bitbucketMessage.getPush().getChanges().get(0).getLinks().getHtml().getHref() + "))";
        StringBuilder embeddedDescription = new StringBuilder();
        for (Commit commit:
             bitbucketMessage.getPush().getChanges().get(0).getCommits()) {
            embeddedDescription.append("[").append(commit.getHash().substring(0, 7)).append("](").append(commit.getLinks().getHtml().getHref()).append("): ").append(commit.getMessage()).append("- ").append(commit.getAuthor().getUser().getDisplayName()).append("\n\n");
        }
//        embeddedDescription.deleteCharAt(embeddedDescription.lastIndexOf("\n")).deleteCharAt(embeddedDescription.lastIndexOf("\n"));
        Embed embed = new Embed("rich", embeddedDescription.toString());
        embeds = new ArrayList<>();
        embeds.add(embed);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Embed> getEmbeds() {
        return embeds;
    }

    public void setEmbeds(List<Embed> embeds) {
        this.embeds = embeds;
    }

    public String toJson(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "fail";
    }
}