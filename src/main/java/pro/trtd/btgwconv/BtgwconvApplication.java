package pro.trtd.btgwconv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtgwconvApplication {

    public static void main(String[] args) {
        SpringApplication.run(BtgwconvApplication.class, args);
    }
}
